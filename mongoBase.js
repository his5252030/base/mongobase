"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoBaseService = void 0;
var mongodb_1 = require("mongodb");
var MongoBaseService = /** @class */ (function () {
    /**
     *  建立連線
     * @param {string} url 連線的mongoDB網址
     * @param {string} databaseName 資料庫的選擇
     * @param {string} collectionName collection的選擇
     */
    function MongoBaseService(url, databaseName, collectionName) {
        this.client = new mongodb_1.MongoClient(url);
        this.databaseName = databaseName;
        this.collectionName = collectionName;
    }
    /**
     * 新增資料
     * @param {Document} document 新增的資料
     * @returns {Promise} 資料庫操作結果
     */
    MongoBaseService.prototype.insertDocument = function (document) {
        return __awaiter(this, void 0, void 0, function () {
            var database, collection, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, , 3, 5]);
                        return [4 /*yield*/, this.client.connect()];
                    case 1:
                        _a.sent();
                        database = this.client.db(this.databaseName);
                        collection = database.collection(this.collectionName);
                        return [4 /*yield*/, collection.insertOne(document)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                    case 3: return [4 /*yield*/, this.client.close()];
                    case 4:
                        _a.sent();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 刪除資料
     * @param {string} id 資料的_id
     * @returns {Promise} 資料庫操作結果
     */
    MongoBaseService.prototype.deleteDocument = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var database, collection, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, , 3, 5]);
                        return [4 /*yield*/, this.client.connect()];
                    case 1:
                        _a.sent();
                        database = this.client.db(this.databaseName);
                        collection = database.collection(this.collectionName);
                        return [4 /*yield*/, collection.deleteOne({
                                _id: new mongodb_1.ObjectId(id),
                            })];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                    case 3: return [4 /*yield*/, this.client.close()];
                    case 4:
                        _a.sent();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 更新資料
     * @param {string} id 資料的_id
     * @param {Document} document 更新的資料
     * @returns {Promise} 資料庫操作結果
     */
    MongoBaseService.prototype.updateDocument = function (id, document) {
        return __awaiter(this, void 0, void 0, function () {
            var database, collection, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, , 3, 5]);
                        return [4 /*yield*/, this.client.connect()];
                    case 1:
                        _a.sent();
                        database = this.client.db(this.databaseName);
                        collection = database.collection(this.collectionName);
                        return [4 /*yield*/, collection.updateOne({ _id: new mongodb_1.ObjectId(id) }, { $set: document })];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                    case 3: return [4 /*yield*/, this.client.close()];
                    case 4:
                        _a.sent();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 查詢資料
     * @param {string} id 資料的_id
     * @returns {Promise} 資料庫查詢結果
     */
    MongoBaseService.prototype.findDocument = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var database, collection, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, , 3, 5]);
                        return [4 /*yield*/, this.client.connect()];
                    case 1:
                        _a.sent();
                        database = this.client.db(this.databaseName);
                        collection = database.collection(this.collectionName);
                        return [4 /*yield*/, collection.findOne({
                                _id: new mongodb_1.ObjectId(id),
                            })];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                    case 3: return [4 /*yield*/, this.client.close()];
                    case 4:
                        _a.sent();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 查詢多筆資料
     * @param {Document} filter 過濾的條件
     * @returns {Promise} 資料庫查詢結果
     */
    MongoBaseService.prototype.findDocuments = function (filter, options) {
        return __awaiter(this, void 0, void 0, function () {
            var database, collection, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, , 3, 5]);
                        return [4 /*yield*/, this.client.connect()];
                    case 1:
                        _a.sent();
                        database = this.client.db(this.databaseName);
                        collection = database.collection(this.collectionName);
                        return [4 /*yield*/, collection.find(filter, options).toArray()];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                    case 3: return [4 /*yield*/, this.client.close()];
                    case 4:
                        _a.sent();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MongoBaseService.prototype.aggregateDocuments = function (pipeline) {
        return __awaiter(this, void 0, void 0, function () {
            var database, collection, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, , 3, 5]);
                        return [4 /*yield*/, this.client.connect()];
                    case 1:
                        _a.sent();
                        database = this.client.db(this.databaseName);
                        collection = database.collection(this.collectionName);
                        return [4 /*yield*/, collection.aggregate(pipeline).toArray()];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                    case 3: return [4 /*yield*/, this.client.close()];
                    case 4:
                        _a.sent();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    return MongoBaseService;
}());
exports.MongoBaseService = MongoBaseService;
var mongoDB = new MongoBaseService("mongodb://localhost:27017", "practice", "order");
/* mongoDB
  .aggregateDocuments([
    { $match: { status: "finished" } },
    { $group: { _id: "$name", total: { $sum: "$amount" } } },
    { $sort: { total: 1 } },
  ])
  .then((v) => console.log(v)); */
/* mongoDB
  .findDocuments({}, { limit: 2, sort: { amount: 1 }, projection: { _id: 0 } })
  .then((v) => console.log(v)); */
mongoDB.deleteDocument("64d5ed72685b7973b073340b");
mongoDB
    .updateDocument("64d49a426bc18af0203339cb", { name: "Roger" })
    .then(function (v) { return console.log(v); });
