# MongoDB (資料庫)

### Module

在 module 的 providers 傳入 MongoBase 用 useValue 進行設定

```ts
import { MongoBaseService } from "./app/mongo-base/mongo-base.service";

providers: [
  {
    provide: MongoBaseService,
    //傳入url,databaseName,collectionName
    useValue: new MongoBaseService("url", "databaseName", "collectionName"),
  },
];
```

### Controller

在 controller 實體化 mongoDB 並操作

```ts
import { MongoBaseService } from '../mongo-base/mongo-base.service';

export class MongoController {

  constructor(private readonly mongoDB: MongoBaseService) {}

  //找單筆資料
  this.mongoDB.findDocument(id)

  //找多筆資料
  this.mongoDB.findDocuments(filter)

  //新增單體資料
  this.mongoDB.insertDocument(document)

  //更新單筆資料
  this.mongodb.updateDocument(id,update)

  //刪除單筆資料
  this.mongodb.deleteDocument(id)
}
```
